# SPDX-FileCopyrightText: 2024 Wesley Schwengle
#
# SPDX-License-Identifier: MIT

FROM registry.gitlab.com/opndev/javascript/docker:latest AS base

COPY package.json package.json
RUN ./dev-bin/docker-npm
#RUN npm i tap --save-dev

COPY . .
RUN npm test
RUN npm run jsdoc

FROM scratch AS pages
COPY --from=base /src/app/out /

