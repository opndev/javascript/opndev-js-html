// SPDX-FileCopyrightText: 2024 Wesley Schwengle
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import { isError }
    from '../lib/index.mjs';

tap.type(isError, 'function',
  "isError() is exported by index.mjs")
