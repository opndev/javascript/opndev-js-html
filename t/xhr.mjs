// SPDX-FileCopyrightText: 2024 Wesley Schwengle
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import { isSuccess, isError, isClientError, isServerError, isRedirect, isInfo } from '../lib/xhr.mjs';

class xhrMock {
  status = 0;

  constructor(code) {
    this.status = code;
  }

}

const ok          = new xhrMock(101);
const success     = new xhrMock(200);
const redirect    = new xhrMock(302);
const clientError = new xhrMock(404);
const serverError = new xhrMock(502);

tap.equal(isInfo(ok), true, "101 is info");
tap.equal(isSuccess(success), true, "200 is success");
tap.equal(isRedirect(redirect), true, "302 is redirect");
tap.equal(isClientError(clientError), true, "404 is client error")
tap.equal(isServerError(serverError), true, "502 is server error");
tap.equal(isError(clientError), true, "404 is an error");
