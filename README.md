# Welcome to @opndev/html

This is a utility function package for HTML based actions. It is an ESM package.

[If you want to know more, have a look at our documenation](https://opndev.gitlab.io/javascript/opndev-js-html/)

## Development

Try to keep functions small and try not to depend on any outside functions. We
aim to keep the dependency graph as small as possible. The use of `esm` is
welcomed and writing tests is encouraged.

### jsdoc

You can build the documentation by running `npm run jsdoc`.

### eslint

The ES Linting profile is flexible and does not try to enforce much. There is
more than one way to do it ([TIMTOWTDI](https://en.wikipedia.org/wiki/There%27s_more_than_one_way_to_do_it)).

Try to stay constistent, but forcing a programming style upon others is bad.

### gitignore

Please educate yourself and use a global gitignore file. Somes files will
always be there. I'm looking at you `node_modules`.

### The lockfiles
You'll also see that `package-lock.json` and/or `yarn.lock` are ignored. I
strongly believe that pinning packages lead to bit rot, as you never update
packages. When a downstream module breaks its API you should know and update
your code accordingly. The same goes for possible upstream modules. If you want
a specific version, pin it to that version, otherwise use the latest and
greatest when possible.

### Code of conduct

There is none, stay human, communicate and accept that some people have
different opinions.

### Pull requests

I'm happy to accept pull requests, just make sure your change has tests.
Add a good commit message and commit with a sign-off `git commit -s`.

A good commit message isn't just `issue(feat): added foo to bar` but also
explain the why in the commit.
