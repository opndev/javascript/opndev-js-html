// SPDX-FileCopyrightText: 2024 Wesley Schwengle
//
// SPDX-License-Identifier: MIT

/**
 *
 * @module module:@opndev/html
 *
 * @exports xhr
 *
 * Export all the useful functions. Unfortunatly, without webpack's
 * require.context we cannot automaticly load everything. So we just keep
 * typing. ES6 requires strict syntax on import and exports :/
 */
export { isInfo, isSuccess, isError, isServerError, isClientError } from './xhr.mjs'
