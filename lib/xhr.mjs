/* SPDX-FileCopyrightText: 2024 Wesley Schwengle
 *
 * SPDX-License-Identifier: MIT
 */

export {
  isSuccess,
  isError,
  isClientError,
  isServerError,
  isRedirect,
  isInfo
}

function isInStatusRange(xhr, code) {
  if (xhr.status >= code && xhr.status < code+100)
    return true;
  return false
}


/**
 * Check if an HTTP status is an info status
 *
 * @function isInfo
 * @param {*} xhr Should support xhr.status
 * @returns {Boolean} true or false
 */
function isInfo(xhr) {
  return isInStatusRange(xhr, 100);
}

/**
 * Check if an HTTP status is a success status
 *
 * @function isSuccess
 * @param {*} xhr Should support xhr.status
 * @returns {Boolean} true or false
 */
function isSuccess(xhr) {
  return isInStatusRange(xhr, 200);
}

/**
 * Check if an HTTP status is a redirect status
 *
 * @function isRedirect
 * @param {*} xhr Should support xhr.status
 * @returns {Boolean} true or false
 */
function isRedirect(xhr) {
  return isInStatusRange(xhr, 300);
}

/**
 * Check if an HTTP status is a client error status
 *
 * @function isClientError
 * @param {*} xhr Should support xhr.status
 * @returns {Boolean} true or false
 */
function isClientError(xhr) {
  return isInStatusRange(xhr, 400);
}

/**
 * Check if an HTTP status is a server error status
 *
 * @function isClientError
 * @param {*} xhr Should support xhr.status
 * @returns {Boolean} true or false
 */
function isServerError(xhr) {
  return isInStatusRange(xhr, 500);
}

/**
 * Check if an HTTP status is an error status. This can be either client or
 * server.
 *
 * @function isError
 * @param {*} xhr Should support xhr.status
 * @returns {Boolean} true or false
 */
function isError(xhr) {
  if (isClientError(xhr) || isServerError(xhr))
    return true;
  return false
}
